from datetime import datetime, timedelta
from threading import Timer

from protocol.Protocol import Protocol
from util.LoggerFactory import LoggerFactory


class WeatherStationController(object):
    RETRY_MINUTES = 5

    @property
    def currentForecast(self):
        return self.__currentForecast

    @property
    def nextFetch(self):
        return self.__nextFetch

    def __init__(self, weatherProvider, arduinoController):
        self.__weatherProvider = weatherProvider
        self.__arduinoController = arduinoController
        self.__currentForecast = None
        self.__logger = LoggerFactory.getLogger(__name__)

    def run(self):
        try:
            currentWeather = self.__weatherProvider.getCurrentWeather()
            self.__logger.info("Received current weather: {}".format(currentWeather.__dict__))
            currentForecast = self.__weatherProvider.getCurrentForecast()
            self.__logger.info("Received current forecast: {}".format(currentForecast.__dict__))

            if currentWeather.dateTimeStart <= currentForecast.dateTimeStart:
                nextForecast = currentForecast
            else:
                nextForecast = self.__weatherProvider.getNextForecast()
                self.__logger.info("Received next forecast: {}".format(nextForecast.__dict__))

            currentWeather.dateTimeEnd = nextForecast.dateTimeStart

            self.__updateWeatherStation(currentWeather, nextForecast)
        except Exception:
            self.__logger.exception("Exception at initial controller run")
            self.__updateNextFetchOnRetry()


    def __updateWeatherStation(self, currentForecast, nextForecast):
        self.__currentForecast = currentForecast
        protocol = Protocol(self.__currentForecast, nextForecast)
        self.__logger.info("Sending: {} to Arduino".format(protocol.asString()))
        try:
            self.__arduinoController.sendMessage(protocol.asString())
        except Exception:
            self.__logger.exception("Error sending message to arduino while updating weather station")
        self.__updateNextFetch()

    def __updateNextFetch(self):
        self.__nextFetch = (self.__currentForecast.dateTimeEnd - self.__currentForecast.dateTimeStart).total_seconds()
        self.__logger.info("Trigger next fetch at: {}".format(self.__currentForecast.dateTimeStart + timedelta(seconds=self.__nextFetch)))
        self.__setTimer()

    def __updateNextFetchOnRetry(self):
        self.__nextFetch = timedelta(minutes=self.RETRY_MINUTES).total_seconds()
        self.__logger.info("Retry next fetch at {}".format(datetime.now() + timedelta(seconds=self.__nextFetch)))
        self.__setTimer()

    def __setTimer(self):
        self.__fetchTimer = Timer(self.__nextFetch, self.__fetchTimerElapsed)
        self.__fetchTimer.start()

    def __fetchTimerElapsed(self):
        try:
            currentForecast = self.__weatherProvider.getCurrentForecast()
            self.__logger.info("Received current forecast: {}".format(currentForecast.__dict__))
            nextForecast = self.__weatherProvider.getNextForecast()
            self.__logger.info("Received next forecast: {}".format(nextForecast.__dict__))

            self.__updateWeatherStation(currentForecast, nextForecast)
        except Exception:
            self.__logger.exception("Exception on timer elapsed")
            self.__updateNextFetchOnRetry()

    def stop(self):
        try:
            self.__fetchTimer.cancel()
        except AttributeError:
            self.__logger.exception("When stopping controller, timer was not initialized")