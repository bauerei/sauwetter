import requests

from .OpenWeatherMapClient import OpenWeatherMapClient

class OpenWeatherMapRestClient(OpenWeatherMapClient):
    def __init__(self, apiKey):
        self.apiKey = apiKey

    def getForecast(self):
        requestParameters = {
            'q': 'Aachen,de',
            'units': 'metric',
            'appid': self.apiKey,
            'cnt': 2
        }
        response = requests.get(
            'https://api.openweathermap.org/data/2.5/forecast',
            params=requestParameters)
        if response.status_code != 200:
            response.raise_for_status()
        return response.json()

    def getCurrentWeather(self):
        requestParameters = {
            'q': 'Aachen,de',
            'units': 'metric',
            'appid': self.apiKey
        }
        response = requests.get(
            'https://api.openweathermap.org/data/2.5/weather',
            params=requestParameters)
        if response.status_code != 200:
            response.raise_for_status()
        return response.json()