import os

from .OpenWeatherMapAdapter import OpenWeatherMapAdapter
from .OpenWeatherMapRestClient import OpenWeatherMapRestClient


class WeatherProviderFactory(object):
    owmTokenEnvironmentVariableName = "OWM_TOKEN"
    
    @staticmethod
    def createWeatherProvider():
        owmToken = \
            os.environ[WeatherProviderFactory.owmTokenEnvironmentVariableName]
        owmClient = OpenWeatherMapRestClient(owmToken)
        return OpenWeatherMapAdapter(owmClient)
