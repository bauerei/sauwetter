import abc


class OpenWeatherMapClient(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def getForecast(self):
        return

    @abc.abstractmethod
    def getCurrentWeather(self):
        return
