from datetime import datetime, timedelta

from .WeatherProvider import WeatherProvider
from .Forecast import Forecast
from .Condition import Condition


class OpenWeatherMapAdapter(WeatherProvider):
    def __init__(self, openWeatherMapClient):
        self.openWeatherMapClient = openWeatherMapClient

    def getCurrentForecast(self):
        jsonResponse = self.openWeatherMapClient.getForecast()
        owmForecast = self.__extractForecast(jsonResponse, 0)
        return self.__convertToForecast(owmForecast)

    def getNextForecast(self):
        jsonResponse = self.openWeatherMapClient.getForecast()
        owmForecast = self.__extractForecast(jsonResponse, 1)
        return self.__convertToForecast(owmForecast)

    def getCurrentWeather(self):
        owmForecast = self.openWeatherMapClient.getCurrentWeather()
        forecast = Forecast()
        forecast.temperatureMax = self.__extractTempMax(owmForecast)
        forecast.temperatureMin = self.__extractTempMin(owmForecast)
        forecast.temperatureAvg = self.__extractTempAvg(owmForecast)
        forecast.condition = self.__extractCondition(owmForecast)
        forecast.dateTimeStart = datetime.now()

        return forecast

    def __convertToForecast(self, owmForecast):
        forecast = Forecast()
        forecast.temperatureMax = self.__extractTempMax(owmForecast)
        forecast.temperatureMin = self.__extractTempMin(owmForecast)
        forecast.temperatureAvg = self.__extractTempAvg(owmForecast)
        forecast.dateTimeStart = self.__extractDateTimeStart(owmForecast)
        forecast.dateTimeEnd = forecast.dateTimeStart + timedelta(hours=3)
        forecast.condition = self.__extractCondition(owmForecast)

        return forecast

    def __extractForecast(self, jsonResponse, index):
        return jsonResponse["list"][index]

    def __extractTempMax(self, owmForecast):
        return owmForecast["main"]["temp_max"]

    def __extractTempMin(self, owmForecast):
        return owmForecast["main"]["temp_min"]

    def __extractTempAvg(self, owmForecast):
        return owmForecast["main"]["temp"]

    def __extractDateTimeStart(self, owmForecast):
        return datetime.strptime(owmForecast["dt_txt"], "%Y-%m-%d %H:%M:%S")

    def __extractCondition(self, owmForecast):
        conditionId = owmForecast["weather"][0]["id"]

        if conditionId >= 200 and conditionId <= 299:
            return Condition.THUNDERSTORM
        elif (conditionId >= 300 and conditionId <= 399) or \
             (conditionId >= 500 and conditionId <= 599):
            return Condition.RAIN
        elif conditionId >= 600 and conditionId <= 699:
            return Condition.SNOW
        elif conditionId >= 700 and conditionId <= 799:
            return Condition.FOG
        elif conditionId == 800:
            return Condition.CLEAR
        elif conditionId >= 801 and conditionId <= 804:
            return Condition.CLOUDS
        else:
            return Condition.UNDEFINED
