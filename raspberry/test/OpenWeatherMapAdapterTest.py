import unittest
import json
from datetime import datetime, timedelta

from unittest.mock import create_autospec
from weather.OpenWeatherMapAdapter import OpenWeatherMapAdapter
from weather.OpenWeatherMapClient import OpenWeatherMapClient
from weather.Forecast import Forecast
from weather.Condition import Condition


class OpenWeatherMapAdapterTest_getCurrentForecast(unittest.TestCase):
    def setUp(self):
        self.any_temperatureAvg = 9.34
        self.any_temperatureMax = 9.34
        self.any_temperatureMin = 5.51
        self.any_condition = Condition.RAIN
        self.any_dateTimeStart = datetime(2018, 12, 22, 12, 0, 0)
        self.any_dateTimeStart_dateTimeEnd = \
            self.any_dateTimeStart + timedelta(hours=3)
        self.any_conditionId = 500

    def test_WhenValidResponseReceived_ReturnsForecast(self):
        # Given
        owmClientFake = create_autospec(spec=OpenWeatherMapClient)
        owmAdapter = OpenWeatherMapAdapter(owmClientFake)
        # When
        owmClientFake.getForecast.return_value = self \
            .getForecast_ValidResponse()
        actual = owmAdapter.getCurrentForecast()
        # Then
        self.assertIsInstance(actual, Forecast)

    def test_WhenValidResponseReceived_ReturnsCorrectTemperatures(self):
        # Given
        owmClientFake = create_autospec(spec=OpenWeatherMapClient)
        owmAdapter = OpenWeatherMapAdapter(owmClientFake)
        # When
        owmClientFake.getForecast.return_value = self \
            .getForecast_ValidResponse()
        actual = owmAdapter.getCurrentForecast()
        # Then
        self.assertTrue(actual.temperatureAvg == self.any_temperatureAvg)
        self.assertTrue(actual.temperatureMax == self.any_temperatureMax)
        self.assertTrue(actual.temperatureMin == self.any_temperatureMin)

    def test_WhenValidResponseReceived_WithRainCondition_ReturnsCorrectCondition(self):
        # Given
        owmClientFake = create_autospec(spec=OpenWeatherMapClient)
        owmAdapter = OpenWeatherMapAdapter(owmClientFake)
        self.any_conditionId = 500
        # When
        owmClientFake.getForecast.return_value = self \
            .getForecast_ValidResponse()
        actual = owmAdapter.getCurrentForecast()
        # Then
        self.assertTrue(actual.condition == Condition.RAIN)

    def test_WhenValidResponseReceived_WithClearCondition_ReturnsCorrectCondition(self):
        # Given
        owmClientFake = create_autospec(spec=OpenWeatherMapClient)
        owmAdapter = OpenWeatherMapAdapter(owmClientFake)
        self.any_conditionId = 800
        # When
        owmClientFake.getForecast.return_value = self \
            .getForecast_ValidResponse()
        actual = owmAdapter.getCurrentForecast()
        # Then
        self.assertTrue(actual.condition == Condition.CLEAR)

    def test_WhenValidResponseReceived_ReturnsCorrectTimes(self):
        # Given
        owmClientFake = create_autospec(spec=OpenWeatherMapClient)
        owmAdapter = OpenWeatherMapAdapter(owmClientFake)
        # When
        owmClientFake.getForecast.return_value = self \
            .getForecast_ValidResponse()
        actual = owmAdapter.getCurrentForecast()
        # Then
        self.assertTrue(actual.dateTimeStart == self.any_dateTimeStart)
        self.assertTrue(
            actual.dateTimeEnd == self.any_dateTimeStart_dateTimeEnd)

    def test_WhenResponseWithMissingKeyReceived_ThrowsError(self):
        # Given
        owmClientFake = create_autospec(spec=OpenWeatherMapClient)
        owmAdapter = OpenWeatherMapAdapter(owmClientFake)
        # When
        owmClientFake.getForecast.return_value = self \
            .getForecast_MissingWeatherKeyResponse()
        # Then
        self.assertRaises(Exception, owmAdapter.getCurrentForecast)

    def test_WhenResponseWithMissingListReceived_ThrowsError(self):
        # Given
        owmClientFake = create_autospec(spec=OpenWeatherMapClient)
        owmAdapter = OpenWeatherMapAdapter(owmClientFake)
        # When
        owmClientFake.getForecast.return_value = self \
            .getForecast_MissingListResponse()
        # Then
        self.assertRaises(Exception, owmAdapter.getCurrentForecast)

    def getForecast_ValidResponse(self):
        jsonResponse = json.loads(
            """
            {
              "cod": "200",
              "message": 0.0068,
              "cnt": 1,
              "list": [
                {
                  "dt": 1545480000,
                  "main": {
                    "temp": 9.34,
                    "temp_min": 5.51,
                    "temp_max": 9.34,
                    "pressure": 1036.74,
                    "sea_level": 1044.66,
                    "grnd_level": 1036.74,
                    "humidity": 91,
                    "temp_kf": 3.83
                  },
                  "weather": [
                    {
                      "id": 500,
                      "main": "Rain",
                      "description": "light rain",
                      "icon": "10d"
                    }
                  ],
                  "clouds": {
                    "all": 12
                  },
                  "wind": {
                    "speed": 5.16,
                    "deg": 279.007
                  },
                  "rain": {
                    "3h": 0.2975
                  },
                  "sys": {
                    "pod": "d"
                  },
                  "dt_txt": "2018-12-22 12:00:00"
                }
              ],
              "city": {
                "id": 2643743,
                "name": "London",
                "coord": {
                  "lat": 51.5085,
                  "lon": -0.1258
                },
                "country": "GB",
                "population": 1000000
              }
            }
            """
        )
        jsonResponse["list"][0]["main"]["temp"] = self.any_temperatureAvg
        jsonResponse["list"][0]["main"]["temp_min"] = self.any_temperatureMin
        jsonResponse["list"][0]["main"]["temp_max"] = self.any_temperatureMax
        jsonResponse["list"][0]["weather"][0]["id"] = self.any_conditionId
        jsonResponse["list"][0]["dt_txt"] = \
            self.any_dateTimeStart.strftime("%Y-%m-%d %H:%M:%S")

        return jsonResponse

    def getForecast_MissingWeatherKeyResponse(self):
        jsonResponse = self.getForecast_ValidResponse()
        return jsonResponse["list"][0].pop("weather")

    def getForecast_MissingListResponse(self):
        jsonResponse = self.getForecast_ValidResponse()
        return jsonResponse.pop("list")

class OpenWeatherMapAdapterTest_getNextForecast(unittest.TestCase):
    def setUp(self):
        self.any_temperatureAvg = -5.2
        self.any_temperatureMax = -5.2
        self.any_temperatureMin = -6.1
        self.any_condition = Condition.CLEAR
        self.any_dateTimeStart = datetime(2018, 12, 31, 12, 0, 0)
        self.any_dateTimeStart_dateTimeEnd = \
            self.any_dateTimeStart + timedelta(hours=3)
        self.any_conditionId = 800

    def test_WhenValidResponseReceived_ReturnsForecast(self):
        # Given
        owmClientFake = create_autospec(spec=OpenWeatherMapClient)
        owmAdapter = OpenWeatherMapAdapter(owmClientFake)
        # When
        owmClientFake.getForecast.return_value = self \
            .getForecast_ValidResponse()
        actual = owmAdapter.getNextForecast()
        # Then
        self.assertIsInstance(actual, Forecast)

    def test_WhenValidResponseReceived_ReturnsCorrectTemperatures(self):
        # Given
        owmClientFake = create_autospec(spec=OpenWeatherMapClient)
        owmAdapter = OpenWeatherMapAdapter(owmClientFake)
        # When
        owmClientFake.getForecast.return_value = self \
            .getForecast_ValidResponse()
        actual = owmAdapter.getNextForecast()
        # Then
        self.assertTrue(actual.temperatureAvg == self.any_temperatureAvg)
        self.assertTrue(actual.temperatureMax == self.any_temperatureMax)
        self.assertTrue(actual.temperatureMin == self.any_temperatureMin)

    def test_WhenValidResponseReceived_WithRainCondition_ReturnsCorrectCondition(self):
        # Given
        owmClientFake = create_autospec(spec=OpenWeatherMapClient)
        owmAdapter = OpenWeatherMapAdapter(owmClientFake)
        self.any_conditionId = 500
        # When
        owmClientFake.getForecast.return_value = self \
            .getForecast_ValidResponse()
        actual = owmAdapter.getNextForecast()
        # Then
        self.assertTrue(actual.condition == Condition.RAIN)

    def test_WhenValidResponseReceived_WithClearCondition_ReturnsCorrectCondition(self):
        # Given
        owmClientFake = create_autospec(spec=OpenWeatherMapClient)
        owmAdapter = OpenWeatherMapAdapter(owmClientFake)
        self.any_conditionId = 800
        # When
        owmClientFake.getForecast.return_value = self \
            .getForecast_ValidResponse()
        actual = owmAdapter.getNextForecast()
        # Then
        self.assertTrue(actual.condition == Condition.CLEAR)

    def test_WhenValidResponseReceived_ReturnsCorrectTimes(self):
        # Given
        owmClientFake = create_autospec(spec=OpenWeatherMapClient)
        owmAdapter = OpenWeatherMapAdapter(owmClientFake)
        # When
        owmClientFake.getForecast.return_value = self \
            .getForecast_ValidResponse()
        actual = owmAdapter.getNextForecast()
        # Then
        self.assertTrue(actual.dateTimeStart == self.any_dateTimeStart)
        self.assertTrue(
            actual.dateTimeEnd == self.any_dateTimeStart_dateTimeEnd)

    def getForecast_ValidResponse(self):
        jsonResponse = json.loads(
            """
            {
              "cod": "200",
              "message": 0.0068,
              "cnt": 2,
              "list": [
                {
                  "dt": 1545480000,
                  "main": {
                    "temp": 9.34,
                    "temp_min": 5.51,
                    "temp_max": 9.34,
                    "pressure": 1036.74,
                    "sea_level": 1044.66,
                    "grnd_level": 1036.74,
                    "humidity": 91,
                    "temp_kf": 3.83
                  },
                  "weather": [
                    {
                      "id": 500,
                      "main": "Rain",
                      "description": "light rain",
                      "icon": "10d"
                    }
                  ],
                  "clouds": {
                    "all": 12
                  },
                  "wind": {
                    "speed": 5.16,
                    "deg": 279.007
                  },
                  "rain": {
                    "3h": 0.2975
                  },
                  "sys": {
                    "pod": "d"
                  },
                  "dt_txt": "2018-12-22 12:00:00"
                },
                {
                  "dt": 1545480000,
                  "main": {
                    "temp": 9.34,
                    "temp_min": 5.51,
                    "temp_max": 9.34,
                    "pressure": 1036.74,
                    "sea_level": 1044.66,
                    "grnd_level": 1036.74,
                    "humidity": 91,
                    "temp_kf": 3.83
                  },
                  "weather": [
                    {
                      "id": 500,
                      "main": "Rain",
                      "description": "light rain",
                      "icon": "10d"
                    }
                  ],
                  "clouds": {
                    "all": 12
                  },
                  "wind": {
                    "speed": 5.16,
                    "deg": 279.007
                  },
                  "rain": {
                    "3h": 0.2975
                  },
                  "sys": {
                    "pod": "d"
                  },
                  "dt_txt": "2018-12-22 12:00:00"
                }
              ],
              "city": {
                "id": 2643743,
                "name": "London",
                "coord": {
                  "lat": 51.5085,
                  "lon": -0.1258
                },
                "country": "GB",
                "population": 1000000
              }
            }
            """
        )
        jsonResponse["list"][1]["main"]["temp"] = self.any_temperatureAvg
        jsonResponse["list"][1]["main"]["temp_min"] = self.any_temperatureMin
        jsonResponse["list"][1]["main"]["temp_max"] = self.any_temperatureMax
        jsonResponse["list"][1]["weather"][0]["id"] = self.any_conditionId
        jsonResponse["list"][1]["dt_txt"] = \
            self.any_dateTimeStart.strftime("%Y-%m-%d %H:%M:%S")

        return jsonResponse

class OpenWeatherMapAdapterTest_getCurrentWeather(unittest.TestCase):
    def setUp(self):
        self.any_temperatureAvg = 9.34
        self.any_temperatureMax = 9.34
        self.any_temperatureMin = 5.51
        self.any_condition = Condition.RAIN
        self.any_conditionId = 500

    def test_WhenValidResponseReceived_ReturnsForecast(self):
        # Given
        owmClientFake = create_autospec(spec=OpenWeatherMapClient)
        owmAdapter = OpenWeatherMapAdapter(owmClientFake)
        # When
        owmClientFake.getCurrentWeather.return_value = self \
            .getForecast_ValidResponse()
        actual = owmAdapter.getCurrentWeather()
        # Then
        self.assertIsInstance(actual, Forecast)

    def test_WhenValidResponseReceived_ReturnsCorrectTemperatures(self):
        # Given
        owmClientFake = create_autospec(spec=OpenWeatherMapClient)
        owmAdapter = OpenWeatherMapAdapter(owmClientFake)
        # When
        owmClientFake.getCurrentWeather.return_value = self \
            .getForecast_ValidResponse()
        actual = owmAdapter.getCurrentWeather()
        # Then
        self.assertTrue(actual.temperatureAvg == self.any_temperatureAvg)
        self.assertTrue(actual.temperatureMax == self.any_temperatureMax)
        self.assertTrue(actual.temperatureMin == self.any_temperatureMin)

    def test_WhenValidResponseReceived_WithRainCondition_ReturnsCorrectCondition(self):
        # Given
        owmClientFake = create_autospec(spec=OpenWeatherMapClient)
        owmAdapter = OpenWeatherMapAdapter(owmClientFake)
        self.any_conditionId = 500
        # When
        owmClientFake.getCurrentWeather.return_value = self \
            .getForecast_ValidResponse()
        actual = owmAdapter.getCurrentWeather()
        # Then
        self.assertTrue(actual.condition == Condition.RAIN)

    def test_WhenValidResponseReceived_WithClearCondition_ReturnsCorrectCondition(self):
        # Given
        owmClientFake = create_autospec(spec=OpenWeatherMapClient)
        owmAdapter = OpenWeatherMapAdapter(owmClientFake)
        self.any_conditionId = 800
        # When
        owmClientFake.getCurrentWeather.return_value = self \
            .getForecast_ValidResponse()
        actual = owmAdapter.getCurrentWeather()
        # Then
        self.assertTrue(actual.condition == Condition.CLEAR)

    def getForecast_ValidResponse(self):
        jsonResponse = json.loads(
            """
            {
                "coord": {
                    "lon": 6.08,
                    "lat": 50.78
                },
                "weather": [
                    {
                        "id": 721,
                        "main": "Haze",
                        "description": "haze",
                        "icon": "50d"
                    },
                    {
                        "id": 701,
                        "main": "Mist",
                        "description": "mist",
                        "icon": "50d"
                    }
                ],
                "base": "stations",
                "main": {
                    "temp": 11.98,
                    "pressure": 1018,
                    "humidity": 87,
                    "temp_min": 7.78,
                    "temp_max": 16.11
                },
                "visibility": 4800,
                "wind": {
                    "speed": 7.2,
                    "deg": 240
                },
                "clouds": {
                    "all": 90
                },
                "dt": 1551356487,
                "sys": {
                    "type": 1,
                    "id": 1525,
                    "message": 0.0063,
                    "country": "DE",
                    "sunrise": 1551334906,
                    "sunset": 1551374123
                },
                "id": 3247449,
                "name": "Aachen",
                "cod": 200
            }
            """
        )
        jsonResponse["main"]["temp"] = self.any_temperatureAvg
        jsonResponse["main"]["temp_min"] = self.any_temperatureMin
        jsonResponse["main"]["temp_max"] = self.any_temperatureMax
        jsonResponse["weather"][0]["id"] = self.any_conditionId

        return jsonResponse