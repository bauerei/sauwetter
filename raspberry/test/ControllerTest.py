import logging
import unittest
from unittest.mock import create_autospec
from datetime import datetime, timedelta

from arduino.ArduinoAdapter import ArduinoAdapter
from weather.Condition import Condition
from weather.Forecast import Forecast
from weather.WeatherProvider import WeatherProvider
from controller.WeatherStationController import WeatherStationController


class ControllerTest_run(unittest.TestCase):

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_WhenCurrentForecastOlderThanCurrentWeather_UpdatesNextFetchToNextForecastStart(self):
        # Given
        weatherProvider = create_autospec(spec=WeatherProvider)
        arduinoController = create_autospec(spec=ArduinoAdapter)
        weatherStationController = WeatherStationController(weatherProvider, arduinoController)
        # When
        weatherProvider.getCurrentForecast.return_value = self.createForecastTimeStartNowTimeEndOneHourLater()
        weatherProvider.getCurrentWeather.return_value = self.createForecastTimeStartNowTimeEndOneHourLater()
        weatherProvider.getNextForecast.return_value = self.createForecastTimeStartNowTimeEndOneHourLater()
        weatherStationController.run()
        weatherStationController.stop()
        # Then
        expectedFetchTime = (weatherProvider.getNextForecast.return_value.dateTimeStart - weatherProvider.getCurrentWeather.return_value.dateTimeStart).total_seconds()
        actualFetchTime = weatherStationController.nextFetch
        self.assertAlmostEqual(expectedFetchTime, actualFetchTime, delta=60)

    def test_WhenCurrentWeatherOlderThanCurrentForecast_UpdatesNextFetchToCurrentForecastStart(self):
        # Given
        weatherProvider = create_autospec(spec=WeatherProvider)
        arduinoController = create_autospec(spec=ArduinoAdapter)
        weatherStationController = WeatherStationController(weatherProvider, arduinoController)
        # When
        weatherProvider.getCurrentWeather.return_value = self.createForecastTimeStartNowTimeEndOneHourLater()
        weatherProvider.getCurrentForecast.return_value = self.createForecastTimeStartNowTimeEndOneHourLater()
        weatherProvider.getNextForecast.return_value = self.createForecastTimeStartNowTimeEndOneHourLater()
        weatherStationController.run()
        weatherStationController.stop()
        # Then
        expectedFetchTime = (weatherProvider.getCurrentForecast.return_value.dateTimeStart - weatherProvider.getCurrentWeather.return_value.dateTimeStart).total_seconds()
        actualFetchTime = weatherStationController.nextFetch
        self.assertAlmostEqual(expectedFetchTime, actualFetchTime, delta=60)

    def test_WhenForecastFetched_SendsForecastToArduino(self):
        # Given
        weatherProvider = create_autospec(spec=WeatherProvider)
        arduinoController = create_autospec(spec=ArduinoAdapter)
        weatherStationController = WeatherStationController(weatherProvider, arduinoController)
        # When
        weatherProvider.getCurrentForecast.return_value = self.createForecastTimeStartNowTimeEndOneHourLater()
        weatherProvider.getCurrentWeather.return_value = self.createForecastTimeStartNowTimeEndOneHourLater()
        weatherProvider.getNextForecast.return_value = self.createForecastTimeStartNowTimeEndOneHourLater()
        weatherStationController.run()
        weatherStationController.stop()
        # Then
        self.assertTrue(arduinoController.sendMessage.called)

    def test_WhenForecastFetched_UpdatesCurrentForecast(self):
        # Given
        weatherProvider = create_autospec(spec=WeatherProvider)
        arduinoController = create_autospec(spec=ArduinoAdapter)
        weatherStationController = WeatherStationController(weatherProvider, arduinoController)
        # When
        weatherProvider.getCurrentForecast.return_value = self.createForecastTimeStartNowTimeEndOneHourLater()
        weatherProvider.getCurrentWeather.return_value = self.createForecastTimeStartNowTimeEndOneHourLater()
        weatherProvider.getNextForecast.return_value = self.createForecastTimeStartNowTimeEndOneHourLater()
        weatherStationController.run()
        weatherStationController.stop()
        # Then
        expectedForecastDict = self.createForecastTimeStartNowTimeEndOneHourLater()
        actualForecastDict = weatherStationController.currentForecast
        self.assertEqual(expectedForecastDict.condition, actualForecastDict.condition)
        self.assertEqual(expectedForecastDict.temperatureMin, actualForecastDict.temperatureMin)
        self.assertEqual(expectedForecastDict.temperatureMax, actualForecastDict.temperatureMax)
        self.assertEqual(expectedForecastDict.temperatureAvg, actualForecastDict.temperatureAvg)

    def test_WhenWeatherProviderThrowsError_NextFetchSetToRetryMinutes(self):
        # Given
        weatherProvider = create_autospec(spec=WeatherProvider)
        arduinoController = create_autospec(spec=ArduinoAdapter)
        weatherStationController = WeatherStationController(weatherProvider, arduinoController)
        # When
        weatherProvider.getCurrentForecast.side_effect = Exception("Error")
        weatherProvider.getCurrentWeather.side_effect = Exception("Error")
        weatherProvider.getNextForecast.side_effect = Exception("Error")
        weatherStationController.run()
        weatherStationController.stop()
        # Then
        expectedNextFetch = timedelta(minutes=weatherStationController.RETRY_MINUTES).total_seconds()
        self.assertEqual(expectedNextFetch, weatherStationController.nextFetch)

    def test_WhenArduinoControllerThrowsError_UpdatesNextFetchIgnoringError(self):
        # Given
        weatherProvider = create_autospec(spec=WeatherProvider)
        arduinoController = create_autospec(spec=ArduinoAdapter)
        weatherStationController = WeatherStationController(weatherProvider, arduinoController)
        # When
        arduinoController.sendMessage.side_effect = Exception("Error")
        weatherProvider.getCurrentWeather.return_value = self.createForecastTimeStartNowTimeEndOneHourLater()
        weatherProvider.getCurrentForecast.return_value = self.createForecastTimeStartNowTimeEndOneHourLater()
        weatherProvider.getNextForecast.return_value = self.createForecastTimeStartNowTimeEndOneHourLater()
        weatherStationController.run()
        weatherStationController.stop()
        # Then
        expectedFetchTime = (
                    weatherProvider.getCurrentForecast.return_value.dateTimeStart - weatherProvider.getCurrentWeather.return_value.dateTimeStart).total_seconds()
        actualFetchTime = weatherStationController.nextFetch
        self.assertAlmostEqual(expectedFetchTime, actualFetchTime, delta=60)

    def createForecastTimeStartNowTimeEndOneHourLater(self, temp=1):
        datetimeStart = datetime.now()
        datetimeEnd = datetimeStart + timedelta(hours=1)
        return Forecast(temp, temp, temp, datetimeStart, datetimeEnd, Condition.CLEAR)