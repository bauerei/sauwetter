import unittest

from weather.Forecast import Forecast
from weather.Condition import Condition
from protocol.Protocol import Protocol
from protocol.ProtocolError import ProtocolError

class ProtocolTest_createFromForecast(unittest.TestCase):
    def setUp(self):
        self.any_temperature = 5

    def test_WhenTemperatureFallsSignificantlyMinConditionClear_ReturnsTemperatureFallingConditionSunny(self):
        # Given
        currentForecast = Forecast(tempAvg=self.any_temperature, condition=Condition.CLEAR)
        newForecast = Forecast(tempAvg=self.any_temperature - 1)
        # When
        protocol = Protocol(currentForecast, newForecast)
        # Then
        expectedProtocol = "{}-{}-{}".format(
            Protocol.CONDITION_CLEAR, Protocol.CONDITION_CONSTANT, Protocol.TEMPERATURE_FALLING)
        self.assertEqual(expectedProtocol, protocol.asString())

    def test_WhenTemperatureFallsSignificantlyConditionClear_ReturnsTemperatureFallingConditionSunny(self):
        # Given
        currentForecast = Forecast(tempAvg=self.any_temperature, condition=Condition.CLEAR)
        newForecast = Forecast(tempAvg=self.any_temperature - 5)
        # When
        protocol = Protocol(currentForecast, newForecast)
        # Then
        expectedProtocol = "{}-{}-{}".format(
            Protocol.CONDITION_CLEAR, Protocol.CONDITION_CONSTANT, Protocol.TEMPERATURE_FALLING)
        self.assertEqual(expectedProtocol, protocol.asString())

    def test_WhenTemperatureFallsUnsignificantlyMaxConditionClear_ReturnsTemperatureConstantConditionSunny(self):
        # Given
        currentForecast = Forecast(tempAvg=self.any_temperature, condition=Condition.CLEAR)
        newForecast = Forecast(tempAvg=self.any_temperature - 0.9)
        # When
        protocol = Protocol(currentForecast, newForecast)
        # Then
        expectedProtocol = "{}-{}-{}".format(
            Protocol.CONDITION_CLEAR, Protocol.CONDITION_CONSTANT, Protocol.TEMPERATURE_CONSTANT)
        self.assertEqual(expectedProtocol, protocol.asString())

    def test_WhenTemperatureFallsUnsignificantlyMinConditionClear_ReturnsTemperatureConstantConditionSunny(self):
        # Given
        currentForecast = Forecast(tempAvg=self.any_temperature, condition=Condition.CLEAR)
        newForecast = Forecast(tempAvg=self.any_temperature - 0.1)
        # When
        protocol = Protocol(currentForecast, newForecast)
        # Then
        expectedProtocol = "{}-{}-{}".format(
            Protocol.CONDITION_CLEAR, Protocol.CONDITION_CONSTANT, Protocol.TEMPERATURE_CONSTANT)

    def test_WhenTemperatureTurnsNegativeConditionClear_ReturnsTemperatureFallingConditionSunny(self):
        # Given
        currentForecast = Forecast(tempAvg=self.any_temperature, condition=Condition.CLEAR)
        newForecast = Forecast(tempAvg=self.any_temperature * -1)
        # When
        protocol = Protocol(currentForecast, newForecast)
        # Then
        expectedProtocol = "{}-{}-{}".format(
            Protocol.CONDITION_CLEAR, Protocol.CONDITION_CONSTANT, Protocol.TEMPERATURE_FALLING)
        self.assertEqual(expectedProtocol, protocol.asString())

    def test_WhenTemperatureRisesSignificantlyMinConditionClear_ReturnsTemperatureRisingConditionSunny(self):
        # Given
        currentForecast = Forecast(tempAvg=self.any_temperature, condition=Condition.CLEAR)
        newForecast = Forecast(tempAvg=self.any_temperature + 1)
        # When
        protocol = Protocol(currentForecast, newForecast)
        # Then
        expectedProtocol = "{}-{}-{}".format(
            Protocol.CONDITION_CLEAR, Protocol.CONDITION_CONSTANT, Protocol.TEMPERATURE_RISING)
        self.assertEqual(expectedProtocol, protocol.asString())

    def test_WhenTemperatureRisesSignificantlyConditionClear_ReturnsTemperatureRisingConditionSunny(self):
        # Given
        currentForecast = Forecast(tempAvg=self.any_temperature, condition=Condition.CLEAR)
        newForecast = Forecast(tempAvg=self.any_temperature + 5)
        # When
        protocol = Protocol(currentForecast, newForecast)
        # Then
        expectedProtocol = "{}-{}-{}".format(
            Protocol.CONDITION_CLEAR, Protocol.CONDITION_CONSTANT, Protocol.TEMPERATURE_RISING)
        self.assertEqual(expectedProtocol, protocol.asString())

    def test_WhenTemperatureRisesUnsignificantlyMaxConditionClear_ReturnsTemperatureConstantConditionSunny(self):
        # Given
        currentForecast = Forecast(tempAvg=self.any_temperature, condition=Condition.CLEAR)
        newForecast = Forecast(tempAvg=self.any_temperature + 0.9)
        # When
        protocol = Protocol(currentForecast, newForecast)
        # Then
        expectedProtocol = "{}-{}-{}".format(
            Protocol.CONDITION_CLEAR, Protocol.CONDITION_CONSTANT, Protocol.TEMPERATURE_CONSTANT)
        self.assertEqual(expectedProtocol, protocol.asString())

    def test_WhenTemperatureRisesUnsignificantlyMinConditionClear_ReturnsTemperatureConstantConditionSunny(self):
        # Given
        currentForecast = Forecast(tempAvg=self.any_temperature, condition=Condition.CLEAR)
        newForecast = Forecast(tempAvg=self.any_temperature + 0.1)
        # When
        protocol = Protocol(currentForecast, newForecast)
        # Then
        expectedProtocol = "{}-{}-{}".format(
            Protocol.CONDITION_CLEAR, Protocol.CONDITION_CONSTANT, Protocol.TEMPERATURE_CONSTANT)
        self.assertEqual(expectedProtocol, protocol.asString())

    def test_WhenTemperatureTurnsPositiveConditionClear_ReturnsTemperatureRisingConditionSunny(self):
        # Given
        currentForecast = Forecast(tempAvg=self.any_temperature * -1, condition=Condition.CLEAR)
        newForecast = Forecast(tempAvg=self.any_temperature * -1 + 1)
        # When
        protocol = Protocol(currentForecast, newForecast)
        # Then
        expectedProtocol = "{}-{}-{}".format(
            Protocol.CONDITION_CLEAR, Protocol.CONDITION_CONSTANT, Protocol.TEMPERATURE_RISING)
        self.assertEqual(expectedProtocol, protocol.asString())

    def test_WhenTemperatureConstantConditionClear_ReturnsTemperatureConstantConditionSunny(self):
        # Given
        currentForecast = Forecast(tempAvg=self.any_temperature, condition=Condition.CLEAR)
        newForecast = Forecast(tempAvg=self.any_temperature)
        # When
        protocol = Protocol(currentForecast, newForecast)
        # Then
        expectedProtocol = "{}-{}-{}".format(
            Protocol.CONDITION_CLEAR, Protocol.CONDITION_CONSTANT, Protocol.TEMPERATURE_CONSTANT)
        self.assertEqual(expectedProtocol, protocol.asString())

    def test_WhenTemperatureConstantConditionThunderstorm_ReturnsTemperatureConstantConditionRainy(self):
        # Given
        currentForecast = Forecast(tempAvg=self.any_temperature, condition=Condition.THUNDERSTORM)
        newForecast = Forecast(tempAvg=self.any_temperature)
        # When
        protocol = Protocol(currentForecast, newForecast)
        # Then
        expectedProtocol = "{}-{}-{}".format(
            Protocol.CONDITION_RAIN, Protocol.CONDITION_CONSTANT, Protocol.TEMPERATURE_CONSTANT)
        self.assertEqual(expectedProtocol, protocol.asString())

    def test_WhenTemperatureConstantConditionRain_ReturnsTemperatureConstantConditionRainy(self):
        # Given
        currentForecast = Forecast(tempAvg=self.any_temperature, condition=Condition.RAIN)
        newForecast = Forecast(tempAvg=self.any_temperature)
        # When
        protocol = Protocol(currentForecast, newForecast)
        # Then
        expectedProtocol = "{}-{}-{}".format(
            Protocol.CONDITION_RAIN, Protocol.CONDITION_CONSTANT, Protocol.TEMPERATURE_CONSTANT)
        self.assertEqual(expectedProtocol, protocol.asString())

    def test_WhenTemperatureConstantConditionSnow_ReturnsTemperatureConstantConditionRainy(self):
        # Given
        currentForecast = Forecast(tempAvg=self.any_temperature, condition=Condition.SNOW)
        newForecast = Forecast(tempAvg=self.any_temperature)
        # When
        protocol = Protocol(currentForecast, newForecast)
        # Then
        expectedProtocol = "{}-{}-{}".format(
            Protocol.CONDITION_RAIN, Protocol.CONDITION_CONSTANT, Protocol.TEMPERATURE_CONSTANT)
        self.assertEqual(expectedProtocol, protocol.asString())

    def test_WhenTemperatureConstantConditionClouds_ReturnsTemperatureConstantConditionCloudy(self):
        # Given
        currentForecast = Forecast(tempAvg=self.any_temperature, condition=Condition.CLOUDS)
        newForecast = Forecast(tempAvg=self.any_temperature)
        # When
        protocol = Protocol(currentForecast, newForecast)
        # Then
        expectedProtocol = "{}-{}-{}".format(
            Protocol.CONDITION_CLOUDS, Protocol.CONDITION_CONSTANT, Protocol.TEMPERATURE_CONSTANT)
        self.assertEqual(expectedProtocol, protocol.asString())

    def test_WhenTemperatureConstantConditionFog_ReturnsTemperatureConstantConditionCloudy(self):
        # Given
        currentForecast = Forecast(tempAvg=self.any_temperature, condition=Condition.FOG)
        newForecast = Forecast(tempAvg=self.any_temperature)
        # When
        protocol = Protocol(currentForecast, newForecast)
        # Then
        expectedProtocol = "{}-{}-{}".format(
            Protocol.CONDITION_CLOUDS, Protocol.CONDITION_CONSTANT, Protocol.TEMPERATURE_CONSTANT)
        self.assertEqual(expectedProtocol, protocol.asString())

    def test_WhenTemperatureConstantConditionUndefined_ThrowsError(self):
        # Given
        currentForecast = Forecast(tempAvg=self.any_temperature, condition=Condition.UNDEFINED)
        newForecast = Forecast(tempAvg=self.any_temperature)
        # When
        protocol = Protocol(currentForecast, newForecast)
        # Then
        self.assertRaises(ProtocolError, protocol.asString)