import unittest

from util.LoggerFactory import LoggerFactory


class LoggerFactoryTest_getLogger(unittest.TestCase):
    def test_WhenLoggerNameProvided_ReturnsLoggerWithName(self):
        # Given
        anyLoggerName = __name__
        # When
        logger = LoggerFactory.getLogger(anyLoggerName)
        # Then
        self.assertEqual(anyLoggerName, logger.name)

    def test_WhenLoggerNameProvided_ReturnsLoggerWithHandler(self):
        # Given
        anyLoggerName = __name__
        # When
        logger = LoggerFactory.getLogger(anyLoggerName)
        # Then
        self.assertTrue(logger.hasHandlers())
