from time import sleep

from arduino.ArduinoSerialAdapter import ArduinoSerialAdapter
from controller.WeatherStationController import WeatherStationController
from weather.WeatherProviderFactory import WeatherProviderFactory

weatherProvider = WeatherProviderFactory.createWeatherProvider()
arduinoController = ArduinoSerialAdapter("/dev/ttyACM0")
controller = WeatherStationController(weatherProvider, arduinoController)
controller.run()
while True:
    sleep(10)