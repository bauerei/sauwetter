from weather.Condition import Condition
from .ProtocolError import ProtocolError

class Protocol(object):
    CONDITION_CLEAR = 0
    CONDITION_CLOUDS = 1
    CONDITION_RAIN = 2

    CONDITION_CONSTANT = 1

    TEMPERATURE_RISING = 0
    TEMPERATURE_FALLING = 1
    TEMPERATURE_CONSTANT = 2

    TEMPERATURE_DIFFERENCE_SIGNIFICANT = 1

    def __init__(self, currentForecast, newForecast):
        self.currentForecast = currentForecast
        self.newForecast = newForecast

    def asString(self):
        condition = self.__getCondition()
        constant = self.CONDITION_CONSTANT
        temperature = self.__getTemperature()
        return "{}-{}-{}".format(condition, constant, temperature)

    def __getCondition(self):
        if (self.currentForecast.condition == Condition.CLEAR):
            return self.CONDITION_CLEAR
        elif (self.currentForecast.condition == Condition.THUNDERSTORM) or \
                (self.currentForecast.condition == Condition.RAIN) or \
                (self.currentForecast.condition == Condition.SNOW):
            return self.CONDITION_RAIN
        elif (self.currentForecast.condition == Condition.CLOUDS) or \
                (self.currentForecast.condition == Condition.FOG):
            return self.CONDITION_CLOUDS
        else:
            raise ProtocolError("Could not translate {} to a valid protocol value".format(self.currentForecast.condition))

    def __getTemperature(self):
        absTemperatureDiff = self.newForecast.temperatureAvg - self.currentForecast.temperatureAvg
        if (absTemperatureDiff >= self.TEMPERATURE_DIFFERENCE_SIGNIFICANT):
            return self.TEMPERATURE_RISING
        elif (absTemperatureDiff <= -self.TEMPERATURE_DIFFERENCE_SIGNIFICANT):
            return self.TEMPERATURE_FALLING
        else:
            return self.TEMPERATURE_CONSTANT