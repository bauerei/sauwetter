FROM balenalib/armv7hf-alpine-python:3.5.6-3.8-run

RUN [ "cross-build-start" ]
# Enable timezones
RUN apk add --no-cache tzdata
ENV TZ Europe/Berlin

# Install required tools
RUN pip3 install pipenv

# Install project dependencies
WORKDIR /Sauwetter
COPY Pipfile Pipfile.lock ./
RUN pipenv install --system --deploy --ignore-pipfile

RUN [ "cross-build-end" ]

# Copy required project files
COPY raspberry/ ./


ENV OWM_TOKEN ""
CMD ["python3", "-m", "main.Start"]